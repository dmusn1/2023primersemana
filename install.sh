#!bin/bash

volumenbasededatos="postgres-data"
volumenpgadmin="pgadmin-data"

if [ -d $volumenbasededatos ]
then
    echo "Existe Volumen de base de datos"
    rm -Rf $volumenbasededatos
else
    echo "no existe volumen"

fi

if [ -d $volumenpgadmin ] 
then
    echo "Existe Volumen de pgadmin"
    rm -Rf $volumenpgadmin

else
    echo "no existe volumen"

fi

echo "Ejecutar Docker Compose"

sudo docker stop dbumg
sudo docker stop pgadmin4_container

sudo docker rm dbumg
sudo docker rm pgadmin4_container

docker-compose up -d 

ls -lha
sudo chmod -R 777 postgres-data pgadmin-data
ls -lha
ls -lha
